
DROP TABLE IF EXISTS user;
CREATE TABLE user(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    email VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    password VARCHAR(255),
    role VARCHAR(255)
) DEFAULT CHARSET UTF8 COMMENT '';
    

DROP TABLE IF EXISTS comment;


CREATE TABLE comment(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    message VARCHAR(255) NOT NULL,
    user_id int NOT NULL,
    product_id int
)DEFAULT CHARSET UTF8 COMMENT '';

DROP TABLE IF EXISTS category;
CREATE TABLE category(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    name VARCHAR(255) NOT NULL
)DEFAULT CHARSET UTF8 COMMENT '';

INSERT INTO category (name) VALUES ('Chocolat');
INSERT INTO category (name) VALUES ('Fraise');

DROP TABLE IF EXISTS ingredient;

CREATE TABLE ingredient(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    name VARCHAR(255) NOT NULL,
    image VARCHAR(255) NOT NULL,
    product_id INT
)DEFAULT CHARSET UTF8 COMMENT '';

INSERT INTO ingredient (name,image, product_id) VALUES ("Farine", "https://cdn.discordapp.com/attachments/832254452613906433/931135363739697192/Farine.png",2),
("Oeuf","https://cdn.discordapp.com/attachments/832254452613906433/931135363123122236/oeuf.png",2),
("Chocolat","https://cdn.discordapp.com/attachments/832254452613906433/931135363399970816/chocolat.png",2);
DROP TABLE IF EXISTS product;

CREATE TABLE product(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    date DATE,
    image VARCHAR(255) NOT NULL,
    category_id INT NOT NULL
)DEFAULT CHARSET UTF8 COMMENT '';

INSERT INTO product (name, description, date, image, category_id) VALUES ('Charlotte au fraise','Mon gateau préféré','2001-08-21','https://images.unsplash.com/photo-1589660117682-0728d988a2d2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8ZnJhaXNlJTIwZ2F0ZWF1fGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',2),
('Brownie','Gateau chocolat carré','2021-10-01','https://images.unsplash.com/photo-1588539543889-20cc7ce4df55?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YnJvd25pZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',1);

DROP TABLE IF EXISTS recette;
CREATE TABLE recette(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255)
) DEFAULT CHARSET UTF8 COMMENT '';

INSERT INTO recette (name,description) VALUES ('Brownie','Jolie gateau chocolat'), ('Gateau a la fraise','random description')