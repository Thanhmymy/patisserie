import { Router } from "express";
import { findAllCategory, findByCategory } from "../repository/category-repository";


export const categoryController = Router();

categoryController.get('/', async (req,res) =>{
    let category = await findAllCategory();
    res.json(category)
})

categoryController.get('/:id', async (req,res)=>{
    let category = await findByCategory(req.params.id, true);
    if (!category) {
        res.status(404).json({ error: 'pas trouvé' });
        return;
    }
    res.json(category)
})


