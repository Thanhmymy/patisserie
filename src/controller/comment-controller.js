import { Router } from "express";
import { addComment,deleteComment, commentToProduct, findAllComment, findByComment } from "../repository/comment-repository";
import passport from "passport";
import { findByUser } from "../repository/user-repository";

export const commentController = Router()


commentController.get('/', async (req,res) =>{
    let comment = await findAllComment();
    res.json(comment)
})

commentController.get('/:id', async (req,res)=>{
    let comment = await findByComment(req.params.id);
    comment.user = await findByUser(req.params.id)
    if (!comment) {
        res.status(404).json({ error: 'pas trouvé' });
        return;
    }
    res.json(comment)
})

commentController.post('/', passport.authenticate('jwt', {session:false}) , async (req,res)=>{
    
    try {
        if(req.user.role == 'admin'|| 'user'){
            await addComment(req.body,req.user.id)
            res.status(201).json(req.body)
        }else{
            res.status(401).end()
        }
    } catch (error) {
        console.log(e);
        res.status(500).end
    }

})

//Table de jointure
commentController.get("/all/:id",async (req,res)=>{
let comment = await commentToProduct(req.params.id)
res.json(comment)
})

commentController.delete('/:id' ,passport.authenticate('jwt', {session:false}), async (req,resp)=>{
    try{
        let toDelete = await findByComment(req.params.id);
        if(!toDelete){
            resp.status(404).end()
            return
        }
        if(req.user.role === 'admin'){
            await deleteComment(toDelete.id)
            resp.status(200).json(req.body)
        }else{
            resp.status(401).end()
        }
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }
})