import { Router } from "express";
import { addPost, deleteProduct, findAllProduct, findById, findByOneCategory } from "../repository/product-repository";
import passport from "passport";
import { addComment, commentToProduct } from "../repository/comment-repository";
import { Comment } from "../entity/Comment";
import { addJoinRecette, findIngredientByProductId } from "../repository/joinrecette-repository";


export const productController = Router();

productController.get('/', async (req, res) => {
    let product = await findAllProduct();
    res.json(product)
})

productController.get('/:id', async (req, res) => {
    let product = await findById(req.params.id, true, true);
    if (!product) {
        res.status(404).json({ error: 'pas trouvé' });
        return;
    }
    res.json(product)
})


productController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, resp) => {
    try {
        let toDelete = await findById(req.params.id);
        if (!toDelete) {
            resp.status(404).end()
            return
        }
        if (req.user.role === 'admin') {
            await deleteProduct(toDelete.id)
            resp.status(200).json(req.body)
        } else {
            resp.status(401).end()
        }
    } catch (e) {
        console.log(e)
        resp.status(500).end()
    }
})

productController.post('/add', passport.authenticate('jwt', { session: false }), async (req, resp) => {
    try {
        if (req.user.role === 'admin') {
            const post = await addPost(req.body)
            await addJoinRecette(req.body.ingredient, post)
            resp.status(200).json(req.body)
        } else {
            resp.status(401).end()
        }
    } catch (e) {
        console.log(e)
        resp.status(500).end()
    }
})


productController.post('/:id/comment', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {

        const product = await findById(req.params.id);

        if (!product) {
            res.status(404).end()
            return
        }

        let comment = new Comment()
        Object.assign(comment, req.body)
        comment.user = req.user
        comment.product = product

        await addComment(req.body)
        res.status(201).json(comment)


    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})
//Controller de la jointure 
productController.get('/all/:id', async (req, res) => {
    let product = await findByOneCategory(req.params.id)
    res.json(product)
})