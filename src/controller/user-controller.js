import { Router } from "express";
import { User, userSchema } from "../entity/User";
import { findAllUser, findByUser, UserRepository } from "../repository/user-repository"
import bcrypt from 'bcrypt';
import { generateToken, protect } from "../utils/token";
import passport from "passport";


export const userController = Router();

userController.post('/register', async (req, res) => {
    try {



        const newUser = new User();
        Object.assign(newUser, req.body);

        const exists = await UserRepository.findByEmail(newUser.email);
        if (exists) {
            res.status(400).json({ error: 'Email déja pris' });
            return;
        }

        newUser.role = 'user'
        newUser.password = await bcrypt.hash(newUser.password, 11);

        await UserRepository.add(newUser);
        res.status(201).json({
            user:newUser,
            token: generateToken({
                email: newUser.email,
                name: newUser.name,
                role: newUser.role,
                id: newUser.id
            })
        })

        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    });


userController.post('/login', async (req, res) => {
    try {

        const user = await UserRepository.findByEmail(req.body.email);
        if (user) {
            console.log(user);
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if (samePassword) {
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        name: user.name,
                        role: user.role,
                        id: user.id
                    })
                });
                return;
            }
        }
        res.status(401).json({ error: 'Pas bon mail ou mdp' });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});


userController.get('/account', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.json(req.user);
});


userController.get('/', protect(), async (req, res) => {

    try {
        const users = await UserRepository.findAll();
        res.json(users)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});


userController.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.json({ message: "hello " + req.user.email });
});

userController.get('/all', async (req, resp) => {
    let user = await findAllUser();
    resp.json(user)
})
userController.get('/one/:id', async (req, resp) => {
    let user = await findByUser(req.params.id);
    if (!user) {
        resp.status(404).json({ error: 'pas trouvé' });
        return;
    }
    resp.json(user)
})
