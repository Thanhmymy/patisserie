import { Router } from "express";
import { addIngredient, deleteIngredient, findAllIngredient, findByIngredient, findByOneProduct, updateIngredient } from "../repository/ingredient-repository";
import passport from "passport";


export const ingredientController = Router()


ingredientController.get('/', async (req,res) =>{
    let ingredient = await findAllIngredient();
    res.json(ingredient)
})

//Controller de la jointure
ingredientController.get('/all/:id', async (req,res)=>{
    let ingredient = await findByOneProduct(req.params.id)
    res.json(ingredient)
})

ingredientController.get('/:id', async (req,res)=>{
    let ingredient = await findByIngredient(req.params.id);
    if (!ingredient) {
        res.status(404).json({ error: 'pas trouvé' });
        return;
    }
    res.json(ingredient)
})

ingredientController.post('/add' ,passport.authenticate('jwt', {session:false}), async (req,resp)=>{
    try{
        if(req.user.role === 'admin'){
            await addIngredient(req.body)
            resp.status(200).json(req.body)
        }else{
            resp.status(401).end()
        }
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }
})

ingredientController.put('/update/:id', async(req,resp)=>{
    let toUpdate = await findByIngredient(req.params.id);
    if(!toUpdate){
        resp.status(404).end()
        return
    }
    toUpdate.quantity = req.body.quantity
            await updateIngredient(toUpdate)
            resp.status(200).json(toUpdate)
})

ingredientController.delete('/:id' ,passport.authenticate('jwt', {session:false}), async (req,resp)=>{
    try{
        let toDelete = await findByIngredient(req.params.id);
        if(!toDelete){
            resp.status(404).end()
            return
        }
        if(req.user.role === 'admin'){
            await deleteIngredient(toDelete.id)
            resp.status(200).json(req.body)
        }else{
            resp.status(401).end()
        }
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }
})