import Joi from 'joi';

export class User{
    id;
    email;
    name;
    password;
    role;

    constructor(id, email, name, password, role){
        this.id = id;
        this.email = email;
        this.name = name;
        this.password = password;
        this.role = role;
        
    }

    toJSON(){
        return{
            id:this.id,
            email:this.email,
            name:this.name,
            role:this.role,
        }
    }

}
export const userSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(4).required()
});
