export class Ingredient{
    id;
    name;
    image;
    product_id;

    constructor(id, name, image, product_id) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.product_id = product_id;
    }

}