export class Joinrecette{
    id;
    productId;
    ingredientId;
    quantity;
    mesure;
    name;
    image;

    constructor(id,productId,ingredientId,quantity,mesure,name,image){
        this.id = id;
        this.productId = productId;
        this.ingredientId = ingredientId;
        this.quantity = quantity;
        this.mesure = mesure;
        this.name = name;
        this.image = image;
    }
}