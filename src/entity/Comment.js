export class Comment{
    id;
    message;
    user_id;
    product_id;

    constructor(id, message, user_id,product_id) {
        this.id = id;
        this.message = message;
        this.user_id = user_id;
        this.product_id = product_id;
    }

}