import express from 'express';
import cors from 'cors';
import { userController } from './controller/user-controller';
import { configurePassport } from './utils/token';
import passport from 'passport';
import { categoryController } from './controller/category-controller';
import { productController } from './controller/product-controller';
import { ingredientController } from './controller/ingredient-controller';
import { commentController } from './controller/comment-controller';


configurePassport();

export const server = express();

server.use(passport.initialize());

server.use(express.json());
server.use(cors());


server.use('/api/user', userController);
server.use('/api/category', categoryController)
server.use('/api/product', productController)
server.use('/api/ingredient',ingredientController)
server.use('/api/comment',commentController)
