import { Product } from "../entity/Product";
import { Category } from "../entity/Category";
import { connection } from "./connection";
import {  findByOneProduct } from "./ingredient-repository";
import { findByCategory } from "./category-repository";
import { commentToProduct } from "./comment-repository";
import { findIngredientByProductId } from "./joinrecette-repository";

export async function findAllProduct(){
    const [rows]=await connection.query('SELECT * FROM product');
    const product = [];
    for(const row of rows){
        let instance = new Product(row.id,row.name,row.description,row.date,row.image,row.category_id);
        product.push(instance);
    }
    return product;
}

export async function findById(id, withIngredient=false,withComment=false){
    const [rows] = await connection.query ('SELECT * FROM product WHERE id=?',[id]);
    if (rows.length === 1){
        const product = new Product(rows[0].id,rows[0].name,rows[0].description,rows[0].date,rows[0].image,rows[0].category_id);
        if(withIngredient){
            product.ingredient = await findIngredientByProductId(id);
        }
        if(withComment){
            product.comment = await commentToProduct(id)
        }
        return product
    }

    return null;
}
export async function deleteProduct(id){
    const [rows] = await connection.query('DELETE FROM product WHERE id=?',[id])

}
export async function addPost(post){
    let [data]= await connection.query('INSERT INTO product (name,description,date,image,category_id) VALUES (?,?,?,?,?)', [post.name,post.description,post.date,post.image,post.category_id]);
    return post.id = data.insertId
}

//Table de jointure vers ingrédient_id
export async function findByOneCategory(category_id, withCategory){
    const [rows] = await connection.query("SELECT * FROM product WHERE category_id = ?", [category_id]);

    const product=[];
    for (const row of rows){
        let category = new Category()
        category.id = row['id'];
        category.name = row['name']
        category.image = row['image']
        category.description = row['description']
        if (withCategory){
            category.product = await findByCategory(row['category_id'])
        }
        product.push(category)
    }
    return product;
}

