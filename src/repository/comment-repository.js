import { connection } from "./connection";
import { Comment } from "../entity/Comment";
import { Product } from "../entity/Product";
import { findById } from "../repository/product-repository";
import { User } from "../entity/User";

export async function findAllComment(){
    const [rows]=await connection.query('SELECT * FROM comment');
    const comment = [];
    for(const row of rows){
        let instance = new Comment(rows[0].id,row.user_id,row.product_id);
        comment.push(instance);
    }
    return comment;
}

export async function deleteComment(id){
    const [rows] = await connection.query('DELETE FROM comment WHERE id=?',[id])

}

export async function findByComment(id){
    const [rows] = await connection.query ('SELECT * FROM comment WHERE id=?',[id]);
    if (rows.length === 1){
        return new Comment(rows[0].id,rows[0].user_id,rows[0].product_id);
    }
    return null;
}

export async function addComment(post,user_id){
    let [data]= await connection.query('INSERT INTO comment (message,user_id,product_id) VALUES (?,?,?)', [post.message,user_id,post.product_id]);
    return post.id = data.insertId
}

//Table de jointure de Comment qui trouve l'id du product
export async function commentToProduct(product_id, withProduct){
const [rows] = await connection.query('SELECT *, user.name FROM comment LEFT JOIN user ON comment.user_id = user.id WHERE product_id=?',[product_id]);
const comments =[];
for (const row of rows){
    let comment= new Comment(row.id, row.message, row.user_id);
    let user = new User(row.user_id,null, row.name)
    comment.user_id = user
    if(withProduct){
        comment.product = await findById(row['product_id'])
    }
    comments.push(comment)
}
return comments;
}