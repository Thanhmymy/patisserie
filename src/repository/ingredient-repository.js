import { Ingredient } from "../entity/Ingredient";
import { Product } from "../entity/Product";
import { connection } from "./connection";
import { findById } from "../repository/product-repository";

//Find All
export async function findAllIngredient() {
    const [rows] = await connection.query('SELECT * FROM ingredient');
    const ingredient = [];
    for (const row of rows) {
        let instance = new Ingredient(row.id, row.name, row.image, row.product_id);
        ingredient.push(instance);
    }
    return ingredient;
}

//Find By Id
export async function findByIngredient(id) {
    const [rows] = await connection.query('SELECT * FROM ingredient WHERE id=?', [id]);
    if (rows.length === 1) {
        return new Ingredient(rows[0].id, rows[0].name, rows[0].image);
    }
    return null;
}

//Add
export async function addIngredient(post) {
    let [data] = await connection.query('INSERT INTO ingredient (name,image,product_id) VALUES (?,?,?)', [post.name, post.image, post.product_id]);
    return post.id = data.insertId
}


//Delete
export async function deleteIngredient(id) {
    const [rows] = await connection.query('DELETE FROM ingredient WHERE id=?', [id])

}

//Table de jointure de Ingredient qui trouve l'Id du product
export async function findByOneProduct(product_id, withProduct) {
    const [rows] = await connection.query("SELECT * FROM ingredient WHERE product_id = ?", [product_id]);
    console.log(product_id);

    const ingredient = [];
    for (const row of rows) {
        let product = new Product();
        product.id = row['id'];
        product.name = row['name']
        product.image = row['image']
        if (withProduct) {
            product.ingredient = await findById(row['product_id']);
        }
        ingredient.push(product)
    }
    return ingredient;
}