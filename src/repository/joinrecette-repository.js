import { Joinrecette } from "../entity/Joinrecette";
import { connection } from "./connection";

export async function findIngredientByProductId(id) {
    const [rows] = await connection.query('SELECT joinrecette.id, productId, ingredientId, joinrecette.quantity, mesure, ingredient.name, ingredient.image FROM joinrecette INNER JOIN ingredient ON joinrecette.ingredientId=ingredient.id WHERE joinrecette.productId=?', [id])
    
    const joinrecette = [];
    for (const row of rows) {
        let instance = new Joinrecette(row.id, row.productId, row.ingredientId, row.quantity, row.mesure, row.name, row.image);
        joinrecette.push(instance);
    }
    return joinrecette;
}

export async function deleteJoinRecette(id){
    const [rows] = await connection.query('DELETE FROM joinrecette WHERE id=?',[id])

}
export async function addJoinRecette(post, id) {
    let records = [] 
    post.map((item)=>records.push([id, item.ingredientId, item.quantity, item.mesure]))
    let [data] = await connection.query('INSERT INTO joinrecette (productId,ingredientId,quantity,mesure) VALUES ?', [records] );
    return post.id = data.insertId
}

export async function updateJoinRecette(parJoinRecette) {
    const [rows] = await connection.query('UPDATE joinrecette SET quantity=? mesure=? WHERE id=?', [parJoinRecette.quantity, parJoinRecette.mesure,parJoinRecette.id])
}
