import { User } from "../entity/User";
import { connection } from "./connection";


export class UserRepository {
    /**
     * 
     * @param {User} user
     */
    static async add(user) {
        const [rows] = await connection.query('INSERT INTO user (email,name,password,role) VALUES (?,?,?,?)', [user.email, user.name, user.password, user.role]);
        user.id= rows.insertId;
    }
    /**
     * 
     * @param {string} email 
     * @returns {Promise<User>} 
     */
    static async findByEmail(email) {
        const [rows] = await connection.query('SELECT * FROM user WHERE email=?', [email]);
        if(rows.length === 1) {
            return new User(rows[0].id, rows[0].email, rows[0].name, rows[0].password, rows[0].role);
        }
        return null;

    }

}
export async function findAllUser(){
    const [rows]=await connection.query('SELECT * FROM user');
    const user = [];
    for(const row of rows){
        let instance = new User(row.email,row.name,row.password,row.role,row.id);
        user.push(instance);
    }
    return user;
}

export async function findByUser(id){
    const [rows] = await connection.query ('SELECT * FROM user WHERE id=?',[id]);
    if (rows.length === 1){
        return new User(rows[0].id,rows[0].email,rows[0].name,rows[0].password,rows[0].role);
    }
    return null;
}