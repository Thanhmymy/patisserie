import { Category } from "../entity/Category";
import { connection } from "./connection";
import { findByOneCategory } from "./product-repository";


export async function findAllCategory(){
    const [rows]=await connection.query('SELECT * FROM category');
    const category = [];
    for(const row of rows){
        let instance = new Category(row.id,row.name);
        category.push(instance);
    }
    return category;
}
//Table de jointure
export async function findByCategory(id,withProduct=false){
    const [rows] = await connection.query ('SELECT * FROM category WHERE id=?',[id]);
    if (rows.length === 1){
        const category= new Category(rows[0].id,rows[0].name);
        if(withProduct){
            category.product = await findByOneCategory(id);
            console.log(category.product)
        }
    return category
    }

    return null;
}